﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 public abstract class CharacterProperties : ICharacter
 {
    //Private Variables
    private readonly string characterImage;


    //Public Functions
    public CharacterProperties(string _characterImage) //Constructor
    {
        this.characterImage = _characterImage;
    }

    //Override Functions
    public string GetCharacterImage() { return characterImage; }

 }
