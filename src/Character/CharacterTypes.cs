﻿using System.Collections;
using System.Collections.Generic;

public class FirstCharacter: CharacterProperties
{
    private const string characterImage = "Character1";
    public FirstCharacter() : base(characterImage) { }
}

public class SecondCharacter : CharacterProperties
{
    private const string characterImage = "Character2";
    public SecondCharacter() : base(characterImage) { }
}

public class ThirdCharacter : CharacterProperties
{
    private const string characterImage = "Character3";
    public ThirdCharacter() : base(characterImage) { }
}
