﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XEnemy : EnemyProperties
{
    private const double enemyHealth = 1000;
    private const float enemySpeed = 0.5f;
    private const string enemyImage = "XEnemy";
    public XEnemy() : base(enemySpeed, enemyHealth, enemyImage) { }
}

public class YEnemy : EnemyProperties
{
    private const double enemyHealth = 1500;
    private const float enemySpeed = 0.1f;
    private const string enemyImage = "YEnemy";

    public YEnemy() : base(enemySpeed, enemyHealth, enemyImage) { }
}

public class ZEnemy : EnemyProperties
{
    private const double enemyHealth = 2000;
    private const float enemySpeed = 0.03f;
    private const string enemyImage = "ZEnemy";

    public ZEnemy() : base(enemySpeed, enemyHealth, enemyImage) { }
}

