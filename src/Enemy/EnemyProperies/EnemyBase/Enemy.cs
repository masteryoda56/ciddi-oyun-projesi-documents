﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IEnemy
{
    //Interface Functions

    double GetEnemyHealth();
    float GetEnemySpeed();
    string GetEnemyImage();

}


