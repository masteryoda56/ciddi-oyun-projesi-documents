﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyProperties : IEnemy
{
    private readonly double enemyHealth;
    private readonly float enemySpeed;
    private readonly string enemyImage;

    public EnemyProperties(float _enemySpeed, double _enemyHealth, string _enemyImage) //Constructor
    {
        this.enemyImage = _enemyImage;
        this.enemySpeed = _enemySpeed;
        this.enemyHealth = _enemyHealth;
    }

    //Override Functions
    public  double GetEnemyHealth() { return enemyHealth; }
    public  float GetEnemySpeed() { return enemySpeed; }
    public  string GetEnemyImage() { return enemyImage; }
}
