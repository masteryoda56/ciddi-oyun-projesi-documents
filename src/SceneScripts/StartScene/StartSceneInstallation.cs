﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using TMPro.Examples;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GlobalEnums;

public class StartSceneInstallation : MonoBehaviour
{
    //SerializeField Variables

    [SerializeField] InitialSettings[] startSettingObjects;
    [SerializeField] TextMeshProUGUI[] startSettingNames;
    [SerializeField] Button[] startSettingButton;
    [SerializeField] TMP_InputField userName;


    [SerializeField] Image difficultyImage;
    [SerializeField] Image weaponImage;
    [SerializeField] Image characterImage;
    [SerializeField] Image lrfImage;

    //Cache Variables
    private GameManagement gameManagementSettings;
    
  
    //Weapon Select
    public void OnClickSelectQLaserWeapon() { SelectedWeapon(GlobalEnums.WeaponNames.QLaserWeapon); }
    public void OnClickSelectWLaserWeapon() { SelectedWeapon(GlobalEnums.WeaponNames.WLaserWeapon); }
    public void OnClickSelectELaserWeapon() { SelectedWeapon(GlobalEnums.WeaponNames.ELaserWeapon); }  
    
    //Character Select
    public void OnClickSelectFirstCharacter() { SelectedCharacter(GlobalEnums.CharacterNames.FirstCharacter); }
    public void OnClickSelectSecondCharacter() { SelectedCharacter(GlobalEnums.CharacterNames.SecondCharacter); }
    public void OnClickSelectThirdCharacter()  { SelectedCharacter(GlobalEnums.CharacterNames.ThirdCharacter); }
    
    //LRF Settings
    public void OnClickSelectLrf60() { SelectedLrf(GlobalEnums.LrfNames.Lrf_60); }
    public void OnClickSelectLrf120() { SelectedLrf(GlobalEnums.LrfNames.Lrf_120); }
    public void OnClickSelectLrf180() { SelectedLrf(GlobalEnums.LrfNames.Lrf_180); }

    //Difficult Settings
    public void OnClickSelectedEasy() { SelectedDifficulty(GlobalEnums.DifficultySettings.Easy); }
    public void OnClickSelectedMedium() { SelectedDifficulty(GlobalEnums.DifficultySettings.Medium); }
    public void OnClickSelectedHard() { SelectedDifficulty(GlobalEnums.DifficultySettings.Hard); }


    //Private Functions
    void Start() { Main(); }
    void Update() { }

    private void Main()
    {
        LoadObjects();
        gameManagementSettings = FindObjectOfType<GameManagement>();
        StartSettings();
    }
    private void StartSettings()
    {
        difficultyImage.sprite = startSettingObjects[6].GetObjectImage;
        weaponImage.sprite = Resources.Load<Sprite>(GlobalFunctions.GetInstance(WeaponNames.QLaserWeapon).GetWeaponImage());
        characterImage.sprite = Resources.Load<Sprite> (GlobalFunctions.GetInstance(CharacterNames.FirstCharacter).GetCharacterImage());
        lrfImage.sprite = Resources.Load<Sprite> (GlobalFunctions.GetInstance(LrfNames.Lrf_60).GetLrfImage());
    }
    private void LoadObjects()
    {
        for (int i = 0; i < startSettingButton.Length; ++i)
        {
            startSettingNames[i].text = startSettingObjects[i].GetObjectName;
            startSettingButton[i].image.sprite = startSettingObjects[i].GetObjectImage;
        }
    }


    //GameManagement and dynamic image settings
    private void SelectedWeapon(GlobalEnums.WeaponNames selectedWeapon)
    {
        gameManagementSettings.SelectedWeapon = selectedWeapon;
        weaponImage.sprite = Resources.Load<Sprite>(GlobalFunctions.GetInstance(selectedWeapon).GetWeaponImage());
    }
    private void SelectedCharacter(GlobalEnums.CharacterNames selectedCharacter)
    {
        gameManagementSettings.SelectedCharacter = selectedCharacter;
        characterImage.sprite = Resources.Load<Sprite>(GlobalFunctions.GetInstance(selectedCharacter).GetCharacterImage());
    }
    private void SelectedDifficulty(GlobalEnums.DifficultySettings selectedDifficulty)
    {
        gameManagementSettings.DifficultySetting = selectedDifficulty;
        difficultyImage.sprite = startSettingObjects[(int)selectedDifficulty + 6].GetObjectImage;
    }
    private void SelectedLrf(GlobalEnums.LrfNames selectedLrf)
    {
        gameManagementSettings.SelectedLrf = selectedLrf;
        lrfImage.sprite = Resources.Load<Sprite>(GlobalFunctions.GetInstance(selectedLrf).GetLrfImage());
    }

    //UserName Setting
    public void ChangeUserName()
    { 
        gameManagementSettings.UserName = userName.text;
    }
    //Scene Loader
    public void GameSceneLoad()
    {
        SceneManager.LoadScene("GameScene");
    }


}
