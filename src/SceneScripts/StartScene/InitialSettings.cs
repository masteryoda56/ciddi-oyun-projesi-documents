﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(menuName = "Start/StartObjects")]
public class InitialSettings : ScriptableObject
{
    //SerializeField Variables
    [SerializeField] Sprite objectImage;
    [SerializeField] string objectName;

    //Public Properties
    public Sprite GetObjectImage { get { return objectImage; } private set { } }
    public string GetObjectName { get { return objectName; } private set { } }
   
}


