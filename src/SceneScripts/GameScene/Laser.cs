﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Laser : MonoBehaviour
{
    //SerializeField Variables
    [SerializeField] Transform startPosition;

    //Private Variables
    private Vector3 worldPoint;
    private const float shootingSpeed = 20f;
    private bool isGetMousePosition = false;
    private int laserRangeLimit;
    private double laserPower;
    
    //Public Properties
    public double LaserPower { get { return laserPower; } private set { } }

    //Public Functions
    public void LoadLaserPower(double currentPower, int range)
    {
        laserPower = currentPower;
        laserRangeLimit = range;
    }

    //Private Functions
    void Start() { Main(); }
    void Update() { ShootingMove(); }

    private void Main() 
    {
        FirstPosition();
        LaserRotation(); 
    }

    private void FirstPosition()
    {
        transform.position = startPosition.position;
    }

    private void LaserRotation()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = Input.mousePosition - pos;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void ShootingMove()
    {

        if (!isGetMousePosition)
            GetMousePosition();

        var targetPosition  = new Vector2(worldPoint.x, worldPoint.y);
        var instantMovement = shootingSpeed * Time.deltaTime;
        transform.position  = Vector2.MoveTowards(transform.position, targetPosition, instantMovement);

        if ((transform.position.x == targetPosition.x && transform.position.y == targetPosition.y) || isLimit(transform.position, startPosition.position))
        {
            TotalScores.TotalShooting++;
            DestroyLaser();
        }

    }

    private void DestroyLaser() { Destroy(gameObject); }
    private void GetMousePosition()
    {
        worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        isGetMousePosition = true;
    }

    private bool isLimit(Vector2 firstPosition, Vector2 secondPosition)
    {         
        return Vector2.Distance(firstPosition, secondPosition) > laserRangeLimit ? true : false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name != "GameStatus")
        {
            TotalScores.TotalSuccesShooting++;
            DestroyLaser();
        }
    }


}
