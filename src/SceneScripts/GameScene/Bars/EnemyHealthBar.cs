﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
    //Private Variables
    private Vector3 localScale;
    private double enemyHealth;
    private double enemyMaxHealth;

    //Public Properties
    public double EnemyHealth { get { return enemyHealth; } set { enemyHealth = value; } }
    public double EnemyMaxHealth { get { return enemyMaxHealth; } set { enemyMaxHealth = value; } }

    //Public Functions
    void Start() { Main(); }
    void Update() { UpdateEnemyHealth(); }

    //Private Functions
    private void Main()
    {
        localScale = transform.localScale;
        enemyHealth = enemyMaxHealth;
    }
    private void UpdateEnemyHealth()
    {
        localScale.x = (float)(enemyHealth / enemyMaxHealth) * 2.5f;
        transform.localScale = localScale;
    }
}
