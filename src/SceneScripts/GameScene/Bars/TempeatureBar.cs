﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;

public class TempeatureBar : MonoBehaviour
{
    //Private Variables
    Vector3 localScale;
    private System.Timers.Timer coolingTimer;
    private bool isHeat = false;
    private float currentTemperatureTime = 0f;

    //Cache Variables
    private WeaponActions weaponTemperature;

    //Public Functions
    void Start() { Main(); }
    void Update()
    {
        if (isHeat)
        {
            TemperatureUpdate();
            ChangeWeaponColor();

            if (currentTemperatureTime <= 0)
            {
                coolingTimer.Stop();
                isHeat = false;
            }
        }
    }
    public void UpdateTemperatureRatio(double currentWeaponHeatTime)
    {

        if (!isHeat)
        {
            Debug.Log("update");
            currentTemperatureTime = (float)currentWeaponHeatTime;
            Debug.Log(currentTemperatureTime);
            coolingTimer.Start();
            isHeat = true;
        }
    }

    //Private Functions
    private void Main()
    {
        localScale = transform.localScale;
        weaponTemperature = FindObjectOfType<WeaponActions>();
        TimerInit();
        TemperatureUpdate();
    }


    private void ChangeWeaponColor()
    {
        byte weaponColor = currentTemperatureTime * 50 > 255 ? (byte)0 :(byte)(255 - (currentTemperatureTime * 50));
        weaponTemperature.ChangeWeaponColor(weaponColor);
    }
    private void TemperatureUpdate()
    {
        localScale.x = currentTemperatureTime * 1.76f;
        transform.localScale = localScale;       
    }

    private void TimerInit()
    {
        coolingTimer = new System.Timers.Timer(10);
        coolingTimer.Enabled = true;
        coolingTimer.Elapsed += new ElapsedEventHandler(coolingBarTimer_Tick);
        coolingTimer.Stop();
    }

    private void coolingBarTimer_Tick(object source, ElapsedEventArgs e)
    {
        currentTemperatureTime -= 0.01f;
    }
}
