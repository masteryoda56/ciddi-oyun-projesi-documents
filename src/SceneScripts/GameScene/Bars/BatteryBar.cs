﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryBar : MonoBehaviour
{

    //Privae Variables
    private Vector3 localScale;

    //Cache Variables
    private WeaponActions weaponBattery;

    //Public Functions
    void Start() { Main(); }
    void Update() { }

    public void UpdateBatteryRatio(double currentBatteryRatio)
    {
        localScale.x = (float)currentBatteryRatio / 10f;
        transform.localScale = localScale;
    }

    //Private Functions
    private void Main()
    {
        weaponBattery = FindObjectOfType<WeaponActions>();
        localScale = transform.localScale;
    }

}
