﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LrfActions : MonoBehaviour
{
    //Cache Variables
    private GameManagement gameManagementSettings;
    
    //Aggregation Variables
    private ILrf mainLrf;

    //Private Funcsitons
    void Start() { Main(); }
    void Update() { }

    private void Main()
    {
        gameManagementSettings = FindObjectOfType<GameManagement>();
        mainLrf = GlobalFunctions.GetInstance(gameManagementSettings.SelectedLrf);
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(mainLrf.GetLrfDegree());
    }
}
