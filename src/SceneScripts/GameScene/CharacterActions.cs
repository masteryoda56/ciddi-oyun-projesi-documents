﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterActions : MonoBehaviour
{

    //Cache Variables
    private GameManagement gameManagementSettings;
    
    //Aggregation Variables
    private ICharacter mainCharacter;

    //Private Functions
    void Start() { Main(); }
    void Update() { }
 
    private void Main()
    {
        gameManagementSettings = FindObjectOfType<GameManagement>();
        mainCharacter = GlobalFunctions.GetInstance(gameManagementSettings.SelectedCharacter);
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(mainCharacter.GetCharacterImage());
    }
}
