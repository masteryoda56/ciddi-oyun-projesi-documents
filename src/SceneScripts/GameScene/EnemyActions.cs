﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class EnemyActions : MonoBehaviour
{
    //Serialize variable
    [SerializeField] Transform attackCoordinates;
    
    //Cache Variables
    private GameStatus gameRules;
    private GameManagement gameManagement;
    private EnemyHealthBar healthBar;

    //Private Variables
    private float attackSpeed;
    private double health;
    private IEnemy enemyType;


    //Private Functions
    void Start() { Main(); }
    void Update() { AttackMove(); }

    private void Main()
    {
        transform.position = RandomPosition();
        gameRules = FindObjectOfType<GameStatus>();

        EnemyInit();      
    }

    private double DamageIntensity(double laserPower) { return WeaponShootingSystem.FirePower(laserPower, EnemyDestroyRange()); }
    private float EnemyDestroyRange() { return Vector2.Distance(transform.position, attackCoordinates.transform.position); }

    private void AttackMove()
    {
        var targetPosition = attackCoordinates.transform.position;
        var instantMovement = attackSpeed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, instantMovement);
    }

    private IEnemy RandomEnemyCreate()
    {       
        return GlobalFunctions.GetInstance(RandomWithWeighted());
    }

    private GlobalEnums.EnemyNames RandomWithWeighted()
    {
        int randomWeighted = Random.Range(0, 100);
        
        if (randomWeighted < 60)
            return GlobalEnums.EnemyNames.XEnemy;
        else if (randomWeighted >= 60 && randomWeighted < 90)
            return GlobalEnums.EnemyNames.YEnemy;
        else
            return GlobalEnums.EnemyNames.ZEnemy;
    }

    private void EnemyInit()
    {
        enemyType = RandomEnemyCreate();
        attackSpeed = enemyType.GetEnemySpeed();
        health = enemyType.GetEnemyHealth();

        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(enemyType.GetEnemyImage());
        healthBar = GetComponentInChildren<EnemyHealthBar>();
        healthBar.EnemyMaxHealth = health;
    }

    private void DestroyEnemy()
    {
        gameRules.CountOfDestroyedEnemy();
        TotalScores.Score++;
        TotalScores.SetTotalDestroyRange((int)EnemyDestroyRange());
        Destroy(gameObject);
        
    }
    
    private void TakeDamage(double damage)
    {
        health -= damage;
        healthBar.EnemyHealth = health;
        TotalScores.TotalDamage += damage;

        Debug.Log("damamage");
        Debug.Log(damage);
        Debug.Log(TotalScores.TotalDamage);

        if (health <= 0)
            DestroyEnemy();       
    }

    private Vector2 RandomPosition()
    {
        int wall_select = Random.Range(0, 4);
        
        switch (wall_select)
        {
            case 0: return new Vector2(-3f, Random.Range(0f, 24f));           
            case 1: return new Vector2(Random.Range(0f, 32f), 27f);
            case 2: return new Vector2(35f, Random.Range(0f, 24f));
            case 3: return new Vector2(Random.Range(0f, 32f), -3f);
            default: return new Vector2(Random.Range(0f, 16f), Random.Range(0f, 12f)); 
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Laser(Clone)")
            TakeDamage(DamageIntensity(collision.gameObject.GetComponent<Laser>().LaserPower));
    }

}
