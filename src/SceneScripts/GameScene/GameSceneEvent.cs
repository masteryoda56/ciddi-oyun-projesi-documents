﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneEvent : MonoBehaviour
{
    //Serializefield Variables
    [SerializeField] GameObject enemy;

    //Cache Variables
    private GameManagement gameManagement;

    //Private Variables
    private Coroutine enemyCoroutine;
    private GlobalEnums.DifficultySettings difficultSetting;

    //Private Functions
    void Start()
    {
        Main();
        StartCoroutine(CreateEnemy());       
    }

    void Update() { }

    private void Main()
    {
        gameManagement = FindObjectOfType<GameManagement>();
        difficultSetting = gameManagement.DifficultySetting;
    }

    private IEnumerator CreateEnemy()
    {
        while (true)
        {
            Instantiate(enemy);
            yield return new WaitForSeconds(TimeToCreateEnemy());
        }
    }

    private float TimeToCreateEnemy()
    {
        switch(difficultSetting)
        {
            case GlobalEnums.DifficultySettings.Easy:   return Random.Range(3f, 5f);
            case GlobalEnums.DifficultySettings.Medium: return Random.Range(2f, 3f);
            case GlobalEnums.DifficultySettings.Hard:   return Random.Range(1f, 2f);
            default: return 0f;
        }
    }

}
