﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStatus : MonoBehaviour
{

    //SerializeField Variables
    [SerializeField] TextMeshProUGUI soldierName;
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] int maxNumberOfEnemy = 20;
    
    //Private Variables
    private int numberOfDestroyedEnemy = 0;
    
    //Cache Variables
    private GameManagement gameManagementSettings;
    private TotalScores totalScores;

    //Public Variables
    public void CountOfDestroyedEnemy()
    {
        numberOfDestroyedEnemy++;
        score.text = numberOfDestroyedEnemy.ToString();
    }

    //Private Variables
    void Start() { Main(); }
    void Update() { SuccesControl(); }

    private void Main()
    {
        gameManagementSettings = FindObjectOfType<GameManagement>();
        soldierName.text = gameManagementSettings.UserName;

        TotalScores.ClearScroes();
        totalScores = FindObjectOfType<TotalScores>();
        totalScores.LevelTimerStart();
    }

    private void SuccesControl()
    {
        if (numberOfDestroyedEnemy >= maxNumberOfEnemy)
        {
            TotalScores.LevelSuccess = true;
            SceneManager.LoadScene("FinishScene");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name != "Laser(Clone)")
        {
            TotalScores.LevelSuccess = false;
            SceneManager.LoadScene("FinishScene");
        }
    }

}
