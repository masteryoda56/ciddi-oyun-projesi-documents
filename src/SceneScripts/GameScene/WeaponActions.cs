﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class WeaponActions : MonoBehaviour
{
    //SerializeField Variables
    [SerializeField]  GameObject laser;
    
    //Aggregation Variables
    private Weapon mainWeapon;


    //Cache Variables
    private BatteryBar batteryBar;
    private TempeatureBar tempeatureBar;
    private GameManagement gameManagementSettings;

    //Public Functions
    public void ChangeWeaponColor(byte weaponColor)
    {
        gameObject.GetComponent<Renderer>().material.color = new Color32(255, weaponColor, weaponColor, 255);
    }

    //Private Functions

    void Start() { Main(); Debug.Log("weapon actions"); }
    void Update()
    {
        WeaponRotation();
        LaserShooting();
    }
    private void Main()
    {
   
        batteryBar = FindObjectOfType<BatteryBar>();
        tempeatureBar = FindObjectOfType<TempeatureBar>();
        gameManagementSettings = FindObjectOfType<GameManagement>();
        mainWeapon = GlobalFunctions.GetInstance(gameManagementSettings.SelectedWeapon);
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(mainWeapon.GetWeaponImage());       
    }

    private void  WeaponRotation()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = Input.mousePosition - pos;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void UpdateInfoBars(double currentBattery, double currentWeaponHeatTime)
    {
        batteryBar.UpdateBatteryRatio((currentBattery / mainWeapon.GetWeaponMaxBatteryCapacity()) * 100);
        tempeatureBar.UpdateTemperatureRatio(currentWeaponHeatTime);
    }
    private void LaserShooting()
    {
        if (Input.GetButtonDown("Fire1"))
            mainWeapon.LaserLoad();

        else if (Input.GetButtonUp("Fire1"))
        {
            try
            {
                FireProperties fireProperties = mainWeapon.LaserFire();
                (((GameObject)(Instantiate(laser))).GetComponent<Laser>()).LoadLaserPower(fireProperties.CurrentFireEnergy, mainWeapon.GetWeaponMaxRange());
                UpdateInfoBars(fireProperties.CurrentBattery, fireProperties.CurrentWeaponTemperature);
            }
            catch(LaserCanNotLoadedException ex)
            {
                Debug.Log(ex.Message);
            }
  
        }
    }


}
