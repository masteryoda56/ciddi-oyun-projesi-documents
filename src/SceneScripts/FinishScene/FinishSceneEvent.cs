﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishSceneEvent : MonoBehaviour
{
    
    //Public Functions
    public void OnClickRestart()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void OnClickHome()
    {
        SceneManager.LoadScene("StartScene");
    }
    public void OnClickExit()
    {
        Application.Quit();
    }
}
