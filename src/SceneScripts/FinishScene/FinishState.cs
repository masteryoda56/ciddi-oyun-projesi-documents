﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FinishState : MonoBehaviour
{
    //SerializeField Variables
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] TextMeshProUGUI totalShooting;
    [SerializeField] TextMeshProUGUI totalSuccesShooting;
    [SerializeField] TextMeshProUGUI totalDamage;
    [SerializeField] TextMeshProUGUI totalDestroyRange;
    [SerializeField] TextMeshProUGUI totalSecond;

    //Cache Variables
    private TotalScores totalScores;

    //Private Functions
    void Start() {  Main(); }

    private void Main()
    {
        SetScores();

        totalScores = FindObjectOfType<TotalScores>();
        totalScores.LevelTimerStop();
        GetDestroyRange();
    }

    private void SetScores()
    {
        score.text = TotalScores.Score.ToString();
        totalShooting.text = (TotalScores.TotalShooting + TotalScores.TotalSuccesShooting).ToString();
        totalSuccesShooting.text = TotalScores.TotalSuccesShooting.ToString();
        totalDamage.text = ((int)TotalScores.TotalDamage).ToString();
        totalSecond.text = ((int)TotalScores.TotalSecond).ToString();
    }

    private void GetDestroyRange()
    {
        ArrayList destroyRanges = TotalScores.GetTotalDestroyRange();
        int enemyRangeCounter = 1;
        foreach(int x in destroyRanges)
            totalDestroyRange.text = totalDestroyRange.text + "\n" + (enemyRangeCounter++) + ". Enemy : " + x + "unit";

        TotalScores.ClearDestroyRanges();
    }

}
