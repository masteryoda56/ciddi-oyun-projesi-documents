﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalEnums
{
    public enum DifficultySettings : int { Easy = 0, Medium = 1, Hard = 2 }
    public enum EnemyNames : int { XEnemy = 0, YEnemy = 1, ZEnemy = 2 }
    public enum WeaponNames : int { QLaserWeapon = 0, WLaserWeapon = 1, ELaserWeapon = 2 }
    public enum CharacterNames : int { FirstCharacter = 0, SecondCharacter = 1, ThirdCharacter = 2 }
    public enum LrfNames : int { Lrf_60 = 0, Lrf_120 = 1, Lrf_180 = 2 }
}

class GlobalConstants
{   
    public const int WEAPON_MIN_HEAT = 20;
    public const int WEAPON_MAX_HEAT = 70;
    public const double WEAPON_MAX_DURATION_TIME = 5;
}


class GlobalFunctions
{  
    public static Weapon GetInstance(GlobalEnums.WeaponNames weaponIndex)
    {
        switch (weaponIndex)
        {
            case GlobalEnums.WeaponNames.QLaserWeapon: return new QLaserWeapon();
            case GlobalEnums.WeaponNames.WLaserWeapon: return new WLaserWeapon();
            case GlobalEnums.WeaponNames.ELaserWeapon: return new ELaserWeapon();
            default: throw new ArgumentOutOfRangeException();
        }
    }

    public static IEnemy GetInstance(GlobalEnums.EnemyNames enemyIndex)
    {
        switch (enemyIndex)
        {
            case GlobalEnums.EnemyNames.XEnemy: return new XEnemy();
            case GlobalEnums.EnemyNames.YEnemy: return new YEnemy();
            case GlobalEnums.EnemyNames.ZEnemy: return new ZEnemy();
            default: throw new ArgumentOutOfRangeException();
        }
    }

    public static ICharacter GetInstance(GlobalEnums.CharacterNames characterIndex)
    {
        switch (characterIndex)
        {
            case GlobalEnums.CharacterNames.FirstCharacter: return new FirstCharacter();
            case GlobalEnums.CharacterNames.SecondCharacter: return new SecondCharacter();
            case GlobalEnums.CharacterNames.ThirdCharacter: return new ThirdCharacter();
            default: throw new ArgumentOutOfRangeException();
        }
    }

    public static ILrf GetInstance(GlobalEnums.LrfNames lrfIndex)
    {
        switch (lrfIndex)
        {
            case GlobalEnums.LrfNames.Lrf_60: return new Lrf_60();
            case GlobalEnums.LrfNames.Lrf_120: return new Lrf_120();
            case GlobalEnums.LrfNames.Lrf_180: return new Lrf_180();
            default: throw new ArgumentOutOfRangeException();
        }
    }
}
