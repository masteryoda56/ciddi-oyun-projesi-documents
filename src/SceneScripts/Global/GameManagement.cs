﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;



public class GameManagement : MonoBehaviour
{
    //Private Variables
    private string userName = "";
    private GlobalEnums.DifficultySettings difficultySetting;
    private GlobalEnums.LrfNames lrfTypes = GlobalEnums.LrfNames.Lrf_60;
    private GlobalEnums.WeaponNames weaponType = GlobalEnums.WeaponNames.QLaserWeapon;
    private GlobalEnums.CharacterNames characterType = GlobalEnums.CharacterNames.FirstCharacter;
    

    //Public Variables
    public string UserName { get { return userName; } set { userName = value; } }
    public GlobalEnums.LrfNames SelectedLrf { get { return lrfTypes; } set { lrfTypes = value; } }
    public GlobalEnums.WeaponNames SelectedWeapon {  get { return weaponType; } set  { weaponType = value;  }  }
    public GlobalEnums.CharacterNames SelectedCharacter { get { return characterType; } set { characterType = value; } }
    public GlobalEnums.DifficultySettings DifficultySetting { get { return difficultySetting; } set { difficultySetting = value; } }


    //Private Functions
    private void Awake() { SingletonInitialize(); }

    private void SingletonInitialize()
    {
        //Singleton Pattern
        int numberOfGameManagement = FindObjectsOfType<GameManagement>().Length;
        if (numberOfGameManagement > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
            DontDestroyOnLoad(gameObject);
    }

}
