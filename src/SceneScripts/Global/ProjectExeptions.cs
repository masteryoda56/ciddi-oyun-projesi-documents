﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
class LaserCanNotLoadedException: Exception
{
    public LaserCanNotLoadedException() : base("laser_can_not_loaded") { }

}