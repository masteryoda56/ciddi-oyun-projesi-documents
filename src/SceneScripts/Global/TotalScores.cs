﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Timers;
using UnityEngine;

public class TotalScores : MonoBehaviour
{
    //Private Variables

    private float levelTimer = 0;
    private bool LevelTimerWorking = false;

    //Static Variables
    Stopwatch LevelTimer = new Stopwatch();
    private static int score = 0;
    private static int totalShooting = 0;
    private static int totalSuccesShooting = 0;
    private static double totalDamage = 0;
    private static ArrayList totalDestroyRange = new ArrayList();
    private static float totalSecond = 0;
    private static bool levelSuccess;


    //Public Properties
    public static int Score { get { return score; } set { score = value; } }
    public static int TotalShooting { get { return totalShooting; } set { totalShooting = value; } }
    public static int TotalSuccesShooting { get { return totalSuccesShooting; } set { totalSuccesShooting = value; } }
    public static double TotalDamage { get { return totalDamage; } set { totalDamage = value; } }
    public static float TotalSecond { get { return totalSecond; } set { totalSecond = value; } }
    public static bool LevelSuccess { get { return levelSuccess; } set { levelSuccess = value; } }

    //Public Functions
    public static ArrayList GetTotalDestroyRange() { return totalDestroyRange; }
    public static void SetTotalDestroyRange(int addVal) { totalDestroyRange.Add(addVal); }
    public static void ClearDestroyRanges() { totalDestroyRange.Clear(); }
    public void Update() { WorkTimer(); }
    public static void ClearScroes()
    {
        Score = 0;
        TotalShooting = 0;
        TotalSuccesShooting = 0;
        TotalDamage = 0;
        TotalSecond = 0;

        levelSuccess = false;
    }

    public void LevelTimerStart()
    {
        LevelTimerWorking = true;
        levelTimer = 0;
    }

    public void LevelTimerStop()
    {
        TotalSecond = levelTimer;
        LevelTimerWorking = false;
    }

    //Private Functions
    private void Awake() { SingletonInitialize(); }

    private void WorkTimer()
    {
        if (LevelTimerWorking)
            levelTimer += Time.deltaTime;
    }

    private void SingletonInitialize()
    {
        //Singleton Pattern
        int numberOfTotalScroes = FindObjectsOfType<TotalScores>().Length;
        if (numberOfTotalScroes > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
            DontDestroyOnLoad(gameObject);
    }
}
