﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public struct FireProperties
{
    //Private Variables

    readonly private double currentBattery;
    readonly private double currentTemperature;
    readonly private double currentFireEnergy;

    //Public Properties
    public double CurrentBattery { get { return currentBattery; } private set { } }
    public double CurrentWeaponTemperature { get { return currentTemperature; } private set { } }
    public double CurrentFireEnergy { get { return currentFireEnergy; } private set { } }

    //Public Functions
    public FireProperties(double _currentBattery, double _currentTemperature, double _currentFireEnergy) //Constructor
    {
        this.currentBattery     = _currentBattery;
        this.currentTemperature = _currentTemperature;
        this.currentFireEnergy  = _currentFireEnergy;
    }


}