﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

abstract public class Weapon
{

    //Private Variables
    private WeaponBatterySystem  weaponBatterySystem;
    private WeaponCoolingSystem  weaponCoolingSystem;
    private WeaponShootingSystem weaponShootingSystem; // currently not used.a feature may be added in the future
    private WeaponFireSystem     weaponFireSystem;


    //Public Abstract Properties
    public abstract string GetWeaponImage();
    public abstract int GetWeaponMaxRange();
    public abstract int GetWeaponMaxBatteryCapacity();



    //Public Functions
    public Weapon(int batteryCapacity) { Main(batteryCapacity); } //Constructor
    public void LaserLoad() { weaponFireSystem.StartLoadEnergy(); }

    public FireProperties LaserFire() {

        double loadedEnergyTime = weaponFireSystem.StopLoadEnergy();
        double loadedEnergy = weaponFireSystem.Fire(loadedEnergyTime);

        return CreateLaser(loadedEnergy, loadedEnergyTime);
    }

    //Private Functions
    private void Main(int batteryCapacity)
    {
        weaponBatterySystem = new WeaponBatterySystem(batteryCapacity);
        weaponCoolingSystem = new WeaponCoolingSystem();
        weaponFireSystem = new WeaponFireSystem();

        /* If want to use x, remove it from comment line
        weaponShootingSystem = new WeaponShootingSystem(); */
    }

    private FireProperties CreateLaser(double loadedEnergy, double loadedEnergyTime) 
    {  
        if(weaponBatterySystem.IsWeaponBatteryFull(loadedEnergy) && weaponCoolingSystem.IsWeaponCold())
        {
            return new FireProperties(weaponBatterySystem.UseBattery(loadedEnergy),
                weaponCoolingSystem.CreatedHeat(loadedEnergyTime),
                    loadedEnergy);       
        }

        throw new LaserCanNotLoadedException();           
    }
}
