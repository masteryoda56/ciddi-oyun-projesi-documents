﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class WeaponFireSystem
{
    //Private Variables
    private Stopwatch loadingEnergyDurationTime = new Stopwatch();

    //Public Functions
    public void StartLoadEnergy() { loadingEnergyDurationTime.Start(); }
    public double StopLoadEnergy() { return LoadingEnergyDurationTimeLimit(StopWatchStop()); }
    public double Fire(double loadingEnergyDurationTime) { return LoadedEnergy(loadingEnergyDurationTime); }


    //Private Functions
    private double LoadedEnergy(double loadingDuration) { return Math.Pow(2, loadingDuration) * 100; }

    private double LoadingEnergyDurationTimeLimit(double durationTime)
    {
        return durationTime > GlobalConstants.WEAPON_MAX_DURATION_TIME ? GlobalConstants.WEAPON_MAX_DURATION_TIME : durationTime;
    }

    private double StopWatchStop()
    {
        loadingEnergyDurationTime.Stop();
        double totalSeconds = loadingEnergyDurationTime.Elapsed.TotalSeconds;
        loadingEnergyDurationTime.Reset();

        return totalSeconds;
    }



}
