﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;
using System;

public class WeaponBatterySystem 
{
    //Private Variables
    private double currentBattery;

    public double CurrentBattery { get { return currentBattery; } private set { } }

    //Public Functions
    public WeaponBatterySystem(int mainBatery) 
    { 
        currentBattery = mainBatery;
    }

    public double UseBattery(double energy) { return UpdateWeaponBattery(energy); }
    public bool IsWeaponBatteryFull(double loadingEnergy) { return currentBattery >= loadingEnergy; }

    //Private Functions
    private double UpdateWeaponBattery(double energy) { return currentBattery -= energy; }

}
