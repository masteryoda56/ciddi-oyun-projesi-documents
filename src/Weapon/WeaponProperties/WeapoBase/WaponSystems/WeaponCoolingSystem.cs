﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Timers;

public class WeaponCoolingSystem
{
    
    //Private Variables
    private System.Timers.Timer coolingTimer;
    private double currentTemperature = GlobalConstants.WEAPON_MIN_HEAT;

    //Public properties
    public double CurrentTemperature { get {return DifferenceTemperature() <= 0f ? 0 : CoolingDuration(DifferenceTemperature()); } private set { } }

    //Public Functions
    public WeaponCoolingSystem() { Main(); }     //Constructor

    public bool IsWeaponCold() { return currentTemperature <= GlobalConstants.WEAPON_MIN_HEAT; }

    public double CreatedHeat(double heatingTime) 
    {
        WeaponHeatingSystem(heatingTime);
        return CurrentTemperatureTimeMillis();
    }

    //Private Functions

    private double DifferenceTemperature() { return currentTemperature - GlobalConstants.WEAPON_MIN_HEAT; }
    private double CurrentTemperatureTimeMillis() { return DifferenceTemperature() <= 0f ? 0 : CoolingDuration(DifferenceTemperature()); }
    
    private void Main()
    {
        coolingTimer = new System.Timers.Timer();
        coolingTimer.Enabled = true;
        coolingTimer.Elapsed += new ElapsedEventHandler(CoolingTimer_Tick);
    }
    
    private void WeaponHeatingSystem(double heatingTime)
    {
        currentTemperature += ((GlobalConstants.WEAPON_MAX_HEAT - GlobalConstants.WEAPON_MIN_HEAT) / GlobalConstants.WEAPON_MAX_DURATION_TIME) * heatingTime;
        coolingTimer.Interval = CoolingDuration(currentTemperature - GlobalConstants.WEAPON_MIN_HEAT) * 1000; // * 1000 for ms to second
        coolingTimer.Start();
    }
    
    private double CoolingDuration(double temp)
    {
        return Math.Pow(2, (temp / 10)) / Math.Pow(2, (temp / 20));
    }
    
    //Cooling Timer
    private void CoolingTimer_Tick(object source, ElapsedEventArgs e)
    {
        currentTemperature = GlobalConstants.WEAPON_MIN_HEAT;
        coolingTimer.Stop();
    }
}
