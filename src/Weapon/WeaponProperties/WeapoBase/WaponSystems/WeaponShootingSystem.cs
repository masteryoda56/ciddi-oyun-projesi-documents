﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WeaponShootingSystem
{
    //Public Functions
    public static double FirePower(double energyOfLaser, double range)
    {
        return energyOfLaser - (energyOfLaser * Math.Log(range, 20));
    }
}
