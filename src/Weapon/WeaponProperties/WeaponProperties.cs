﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponProperties : Weapon
{
    //Private Variables
    private readonly string weaponImage;
    private readonly int weaponMaxRange;
    private readonly int weaponBatteryCapacity;

    //Public Functions

    public WeaponProperties(int _laserBatteryCapacity, int _laserMaxRange, string _weaponImage) : base(_laserBatteryCapacity) //Constructor
    {
        this.weaponImage = _weaponImage;
        this.weaponMaxRange = _laserMaxRange;
        this.weaponBatteryCapacity = _laserBatteryCapacity;     
    }

    //Override Functions
    public override string GetWeaponImage() { return weaponImage; }
    public override int GetWeaponMaxRange() { return weaponMaxRange; }
    public override int GetWeaponMaxBatteryCapacity() { return weaponBatteryCapacity; }

}
