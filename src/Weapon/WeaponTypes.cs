﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



 public class QLaserWeapon : WeaponProperties
{
    private const int weaponBatteryCapacity = 5000;
    private const int weaponMaxRange = 5;
    private const string weaponImage = "QLaserWeapon";
    public QLaserWeapon(int garbageVal1 = 0, int garbageVal2 = 0) : base(weaponBatteryCapacity, weaponMaxRange, weaponImage) { }

}

public class WLaserWeapon : WeaponProperties
{
    private const int weaponBatteryCapacity = 10000;
    private const int weaponMaxRange = 15;
    private const string weaponImage = "WLaserWeapon";
    public WLaserWeapon(int garbageVal1 = 0, int garbageVal2 = 0) : base(weaponBatteryCapacity, weaponMaxRange, weaponImage) { }
}

public class ELaserWeapon : WeaponProperties
{
    private const int weaponBatteryCapacity = 15000;
    private const int weaponMaxRange = 20;
    private const string weaponImage = "ELaserWeapon";
    public ELaserWeapon(int garbageVal1 = 0, int garbageVal2 = 0) : base(weaponBatteryCapacity, weaponMaxRange, weaponImage) { }
}
