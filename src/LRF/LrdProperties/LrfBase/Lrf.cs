﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface ILrf
{
    //Interface Functions

    string GetLrfDegree();
    string GetLrfImage();
}
