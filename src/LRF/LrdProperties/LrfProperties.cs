﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class LrfProperties : ILrf
{
    //Private Variables
    private readonly string lrfDegree;
    private readonly string lrfImage;

    //Public Functions
    public LrfProperties(string _lrfDegree, string _lrfImage) //Constructor
    {
        this.lrfDegree = _lrfDegree;
        this.lrfImage = _lrfImage;
    }

    //Override Functions
    public string GetLrfDegree() { return lrfDegree; }
    public string GetLrfImage() { return lrfImage; }
}

