﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lrf_60 : LrfProperties
{
    private const string lrfImage = "Lrf_60";
    private const string lrfDegree = "Lrf_60_degree";

    public Lrf_60() : base(lrfDegree, lrfImage) { }
}

public class Lrf_120 : LrfProperties
{
    private const string lrfImage = "Lrf_120";
    private const string lrfDegree = "Lrf_120_degree";
    public Lrf_120() : base(lrfDegree, lrfImage) { }
}

public class Lrf_180 : LrfProperties
{
    private const string lrfImage = "Lrf_180";
    private const string lrfDegree = "Lrf_180_degree";
    public Lrf_180() : base(lrfDegree, lrfImage) { }
}

